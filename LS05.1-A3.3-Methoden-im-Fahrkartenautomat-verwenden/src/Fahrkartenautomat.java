import java.util.Scanner;

class Fahrkartenautomat
{
	
	/*
	 * 
	 * fahrkartenbestellungErfassen
	 * fahrkartenBezahlen
	 * fahrkartenAusgeben
	 * rueckgeldAusgeben
	 * 
	 */
	
    public static void main(String[] args)
    {
    	double zuZahlen = fahrkartenbestellungErfassen();
    	double bezahlung = fahrkartenBezahlen(zuZahlen);
    	fahrkartenAusgeben();
    	rueckgeldAusgeben(bezahlung, zuZahlen);
    }

    private static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
        
        // Der Betrag der noch gezahlt werden muss.
        // Die Variable ist ein Double weil man auch Cent Betr�ge eingeben kann.
        double zuZahlenderBetrag;
        // Der Betrag der schon bezhalt wurde.
        // Auch hier ein Double weil man Cent Betr�ge zahlen kann.
        double eingezahlterGesamtbetrag;
        // Der Betrag der in den Automaten eingeworfen wurde.
        // Auch hier sind Cent Betr�ge m�glich.
        double eingeworfeneM�nze;
        // Wenn man mehr bezhalt hat als gefordet wird der R�ckgabe Wert in dieser Variable gespeichert.
        // Auch hier sind Cent Betr�ge m�glich.
        double r�ckgabebetrag;
        // Die Anzahl der Tickets die gekauft werden.
        // Der Datentyp ist hier Short weil man vielleicht mehr als 127 Tickets kaufen will.
        short tickets;

        System.out.print("Zu zahlender Betrag (EURO): ");
        zuZahlenderBetrag = tastatur.nextDouble();
        
        System.out.println("Anzahl der Tickets: ");
        tickets = tastatur.nextShort();
        
        // Operator: Multiplikationszuweisung
        zuZahlenderBetrag *= tickets;
        
        return zuZahlenderBetrag;
    }
    
    private static double fahrkartenBezahlen(double zuZahlen) {    	
        // Geldeinwurf
        // -----------
    	
    	Scanner scanner = new Scanner(System.in);
    	
        double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlen - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   
     	   double eingeworfeneM�nze = scanner.nextDouble();
     	   
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
        
        return eingezahlterGesamtbetrag;
        
    }
    
    private static void fahrkartenAusgeben() {

        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
        }
        System.out.println("\n\n");
    	
    }
    
    private static void rueckgeldAusgeben(double gesamtbetrag, double zuZahlen) {

        // R�ckgeldberechnung und -Ausgabe
        // -------------------------------
        double r�ckgabebetrag = gesamtbetrag - zuZahlen;
        if(r�ckgabebetrag > 0.0)
        {
     	   System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " EURO");
     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
            {
         	  muenzeAusgeben(2, "EURO");
 	          r�ckgabebetrag -= 2.0;
            }
            while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
           	  muenzeAusgeben(1, "EURO");
 	          r�ckgabebetrag -= 1.0;
            }
            while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
           	  muenzeAusgeben(50, "CENT");
 	          r�ckgabebetrag -= 0.5;
            }
            while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
           	  muenzeAusgeben(20, "CENT");
  	          r�ckgabebetrag -= 0.2;
            }
            while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
           	  muenzeAusgeben(10, "CENT");
 	          r�ckgabebetrag -= 0.1;
            }
            while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
           	  muenzeAusgeben(5, "CENT");
  	          r�ckgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir w�nschen Ihnen eine gute Fahrt.");
    	
    }
    
    private static void warte(int millis) {

        try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    }
    
    private static void muenzeAusgeben(int betrag, String einheit) {
    	System.out.println(betrag + " " + einheit);
    }
    

}