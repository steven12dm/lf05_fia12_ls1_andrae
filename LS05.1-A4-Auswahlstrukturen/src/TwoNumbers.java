
public class TwoNumbers {
	
	
	public static void main(String[] args) {
		
		// Aufgabe 1: Eigene Bedingungen
		
		int x = 12;
		int y = 8;
		
		if(x == y) {
			System.out.println("Beide Zahlen sind gleich");
		}
		
		if(x > y) {
			System.out.println("x ist gr��er als y");
		}
		
		if(y >= x) {
			System.out.println("y ist gr��er oder gleich gr�� als x");
		} else {
			System.out.println("x ist gr��er als y");
		}
		
		//---------
		
		x = 10;
		y = 18;
		int z = 2;
		
		if(x > y && x > z) {
			System.out.println("x ist gr��er als y und z");
		}
		
		if(z > y || z > x) {
			System.out.println("z ist gr��er als y oder x");
		}
		
		if(x > y && x > z) {
			System.out.println("x ist die gr��te Zahl");
		} else if(y > x && y > z) {
			System.out.println("y ist die gr��te Zahl");
		} else if(z > y && z > x) {
			System.out.println("z ist die gr��te Zahl");
		}
		
	}

}
