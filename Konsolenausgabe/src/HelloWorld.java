
public class HelloWorld {

	public static void main(String[] args) {
		
		//TODO: Edit Types of arguments
		//TODO: Edit Commit Messages
		
		System.out.printf("Name: %-10s Age: %-10d Weight: %-10.2f\n", "Test", 2, 2.2);
		
		
		// Konsolenausgabe �bung 1 Aufgabe 1
		System.out.println("Hello World!!!");
		System.out.println("Das ist ein Beispielsatz. Ein Beispielsatz ist das.\n");
		System.out.println("Das ist ein \"Beispielsatz\".\nEin Beispielsatz ist das.");
		// Das ist ein Kommentar
		
		System.out.printf("Hello %s!%n", "World");
		
		String s = "Java-Programm";
		
		System.out.printf("\n|%7s|\n", s);
		
		printLineBreak();		
		

		// Konsolenausgabe �bung 1 Aufgabe 2
		System.out.printf("%8s\n", "*");
		System.out.printf("%9s\n", "***");
		System.out.printf("%10s\n", "*****");
		System.out.printf("%11s\n", "*******");
		System.out.printf("%12s\n", "*********");
		System.out.printf("%13s\n", "***********");
		System.out.printf("%14s\n", "*************");
		System.out.printf("%9s\n", "***");
		System.out.printf("%9s\n", "***");

		printLineBreak();
		

		// Konsolenausgabe �bung 1 Aufgabe 3
		String numberFormat = "%.2f\n";
		
		System.out.printf(numberFormat, 22.4234234);
		System.out.printf(numberFormat, 111.2222);
		System.out.printf(numberFormat, 4.0);
		System.out.printf(numberFormat, 1000000.551);
		System.out.printf(numberFormat, 97.34);

		printLineBreak();
		

		// Konsolenausgabe �bung 2 Aufgabe 1
		System.out.printf("%11s\n", "**");
		System.out.printf("%7s%7s\n", "*", "*");
		System.out.printf("%7s%7s\n", "*", "*");
		System.out.printf("%11s\n", "**");

		printLineBreak();

		// Konsolenausgabe �bung 2 Aufgabe 2
		System.out.printf("%-5s=%-19s=%4s\n", "0!", " 1", "1");
		System.out.printf("%-5s=%-19s=%4s\n", "1!", " 1", "1");
		System.out.printf("%-5s=%-19s=%4s\n", "2!", " 1 * 2", "2");
		System.out.printf("%-5s=%-19s=%4s\n", "3!", " 1 * 2 * 3", "6");
		System.out.printf("%-5s=%-19s=%4s\n", "4!", " 1 * 2 * 3 * 4", "24");
		System.out.printf("%-5s=%-19s=%4s\n", "5!", " 1 * 2 * 3 * 4 * 5", "120");
		
		printLineBreak();

		// Konsolenausgabe �bung 2 Aufgabe 3 | Temperaturentabelle
		System.out.printf("%-12s|%10s\n", "Fahrenheit", "Celsuis");
		System.out.printf("%.23s\n", "-----------------------");
		System.out.printf("%-12s|%10s\n", "-20", "-28.89");
		System.out.printf("%-12s|%10s\n", "-10", "-23.33");
		System.out.printf("%-12s|%10s\n", "+0", "-17.78");
		System.out.printf("%-12s|%10s\n", "+20", "-6.67");
		System.out.printf("%-12s|%10s\n", "+30", "-1.11");
	}
	
	// Eine statische Methode um die Line Breaks zu printen.
	private static void printLineBreak() {
		System.out.println("\n\n\n\n");
	}
	
	
	
	// Nur ein Test um den Pfeil dynamisch anzeigen zu lassen (Funktioniert aber noch nicht wirkich)
	private static void task() {

		StringBuilder builder = new StringBuilder("*");
		int maxStars = 40;
		int logLength = 2;
		
		for(int spaces = maxStars - 6; spaces <= maxStars + logLength; spaces++) {
			
			if(spaces <= maxStars) {
				System.out.printf("%" + spaces + "s\n" , builder.toString());
				builder.append("**");
				continue;
			}
			
		
			System.out.printf("%" + (maxStars - 5) + "s\n", "***");
			
		}
		
	}

}
