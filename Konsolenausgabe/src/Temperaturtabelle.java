
public class Temperaturtabelle {
	
	public static void main(String[] args) {
		
		// Konsolenausgabe �bung 2 Aufgabe 3 | Temperaturentabelle
		System.out.printf("%-12s|%10s\n", "Fahrenheit", "Celsuis");
		System.out.printf("%.23s\n", "-----------------------");
		System.out.printf("%-12d|%10.2f\n", -20, -28.89);
		System.out.printf("%-12d|%10.2f\n", -10, -23.33);
		
		// Hier habe ich die ersten Zahlen als Strings angegeben, weil die "+" Zeichen nicht angezeigt werden wenn ich sie als Integer �bergebe.
		System.out.printf("%-12s|%10.2f\n", "+0", -17.78);
		System.out.printf("%-12s|%10.2f\n", "+20", -6.67);
		System.out.printf("%-12s|%10.2f\n", "+30", -1.11);
		
		
	}

}
