import java.util.Scanner;

public class Information {
	
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Gebe dein Name ein: ");
		String name = scanner.nextLine();
		
		System.out.println("Gebe dein Alter ein: ");
		short age = scanner.nextShort();
		
		System.out.println(name + " ist " + age + " Jahre alt.");
		
	}
	
	
	

}
