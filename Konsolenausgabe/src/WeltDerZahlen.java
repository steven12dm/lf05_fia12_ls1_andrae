/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    byte anzahlPlaneten = 8;
    
    // Anzahl der Sterne in unserer Milchstra�e
    long anzahlSterne = 200_000_000_000l;
    
    // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = 3_645_000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    short alterTage = 6935;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    float gewichtKilogramm = 150_000;
    
    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
    int flaecheGroessteLand = 17_098_242;
    
    // Wie gro� ist das kleinste Land der Erde?
    
    float flaecheKleinsteLand = .44f;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    
    System.out.println("Bewohner in Berlin: " + bewohnerBerlin);
    
    System.out.println("Ich bin " + anzahlSterne + " Tage alt");
    
    System.out.println("Das schwerste Tier der Welt wiegt: " + gewichtKilogramm);
    
    System.out.println("Das gr��te Land ist: " + flaecheGroessteLand + " km� gro�");
    
    System.out.println("Das kleinste Land ist: " + flaecheKleinsteLand + " km� gro�");
    
    
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}
