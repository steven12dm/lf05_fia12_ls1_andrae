import java.util.Scanner;

class Fahrkartenautomat
{
	
	/*
	 * 
	 * fahrkartenbestellungErfassen
	 * fahrkartenBezahlen
	 * fahrkartenAusgeben
	 * rueckgeldAusgeben
	 * 
	 */
	
    public static void main(String[] args) {
    	
    	while(true) {
        	double zuZahlen = fahrkartenbestellungErfassen();
        	double bezahlung = fahrkartenBezahlen(zuZahlen);
        	fahrkartenAusgeben();
        	rueckgeldAusgeben(bezahlung, zuZahlen);
    	}
    	
    }

    private static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
        
        // Der Betrag der noch gezahlt werden muss.
        // Die Variable ist ein Double weil man auch Cent Betr�ge eingeben kann.
        double zuZahlenderBetrag = 0;
        
        /*
         * Der Vorteil eines Arrays ist die modularit�t. Es ist sehr einfach neue Tickets hinzuzuf�gen und zu entfernen.
         * Die Ausgabe ist auch automatisch und muss nicht noch einmal geschrieben werden.
         */
        // Die verschiedenen Ticketbezeichungen und Preise
        String[] ticketBezeichnungen = {
        		"Einzelfahrschein Berlin AB",
        		"Einzelfahrschein Berlin BC",
        		"Einzelfahrschein Berlin ABC",
        		"Kurzstrecke",
        		"Tageskarte Berlin AB",
        		"Tageskarte Berlin BC",
        		"Tageskarte Berlin ABC",
        		"Kleingruppen-Tageskarte Berlin AB",
        		"Kleingruppen-Tageskarte Berlin BC",
        		"Kleingruppen-Tageskarte Berlin ABC"
        };
        double[] ticketPreise = {
        		2.9,
        		3.3,
        		3.6,
        		1.9,
        		8.6,
        		9,
        		9.6,
        		23.5,
        		24.3,
        		24.9
        };
        
        System.out.println("Fahrkartenbestellvorgan:");
        System.out.println("========================");
        
        while(true) {
            
            // Die Anzahl der Tickets die gekauft werden.
            // Der Datentyp ist hier Short weil man vielleicht mehr als 127 Tickets kaufen will.
            short tickets;
            // Die Art des Tickets
            short ticketType;

            System.out.println("\nW�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:");
            
            for(int i = 0; i < ticketBezeichnungen.length; i++) {
            	
            	String name = ticketBezeichnungen[i];
            	double price = ticketPreise[i];
            	int index = i + 1;
            	System.out.printf("%s [%.2f EUR] (%d)\n", name, price, index);
            	
            }
            
            System.out.println("Bezahlen (-1)");
            System.out.println();

            
            System.out.print("Ihre Wahl: ");
            
            while(((ticketType = (tastatur.nextShort())) < 1 || ticketType > ticketBezeichnungen.length) && ticketType != -1) {
                System.out.println(">>Bitte geben Sie eine g�ltige Nummer ein. Die Auflistung steht am Anfang.<<");
                System.out.print("Ihre Wahl: ");
            }
            
            if(ticketType == -1)
            	return zuZahlenderBetrag;
            
            System.out.print("Anzahl der Tickets: ");       
            
            while((tickets = tastatur.nextShort()) < 1 || tickets > 10) {
                System.out.println(">>W�hlen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.<<\n");
                System.out.print("Anzahl der Tickets: ");
            }
            
            zuZahlenderBetrag += ticketPreise[ticketType - 1] * tickets;
            
            System.out.printf("\nZwischensumme: %.2f �\n", zuZahlenderBetrag);
        	
        }
    }
    
    private static double fahrkartenBezahlen(double zuZahlen) {    	
        // Geldeinwurf
        // -----------
    	
    	Scanner scanner = new Scanner(System.in);
    	
        double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   System.out.printf("Noch zu zahlen: %.2f �\n", (zuZahlen - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   
     	   double eingeworfeneM�nze = scanner.nextDouble();
     	   
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
        
        return eingezahlterGesamtbetrag;
        
    }
    
    private static void fahrkartenAusgeben() {

        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
        }
        System.out.println("\n");
    	
    }
    
    private static void rueckgeldAusgeben(double gesamtbetrag, double zuZahlen) {

        // R�ckgeldberechnung und -Ausgabe
        // -------------------------------
        double r�ckgabebetrag = gesamtbetrag - zuZahlen;
        if(r�ckgabebetrag > 0.0)
        {
     	   System.out.printf("R�ckgabebetrag in H�he von %.2f � wird in folgenden M�nzen gezahlt:\n", r�ckgabebetrag);

            while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
            {
         	  muenzeAusgeben(2, "EURO");
 	          r�ckgabebetrag -= 2.0;
            }
            while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
           	  muenzeAusgeben(1, "EURO");
 	          r�ckgabebetrag -= 1.0;
            }
            while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
           	  muenzeAusgeben(50, "CENT");
 	          r�ckgabebetrag -= 0.5;
            }
            while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
           	  muenzeAusgeben(20, "CENT");
  	          r�ckgabebetrag -= 0.2;
            }
            while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
           	  muenzeAusgeben(10, "CENT");
 	          r�ckgabebetrag -= 0.1;
            }
            while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
           	  muenzeAusgeben(5, "CENT");
  	          r�ckgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein vor Fahrtantritt entwerten zu lassen!");
        System.out.println("Wir w�nschen Ihnen eine gute Fahrt.\n");
    	
    }
    
    private static void warte(int millis) {

        try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    }
    
    private static void muenzeAusgeben(int betrag, String einheit) {
    	System.out.println(betrag + " " + einheit);
    }
    

}