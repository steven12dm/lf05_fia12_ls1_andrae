
public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
      double x = 2.0;
      double y = 4.0;
      double m = calculateAverage(x, y);
	   
	   System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
      
   }
   
   public static double calculateAverage(double x, double y) {
	      return (x + y) / 2;
   }
   
}
